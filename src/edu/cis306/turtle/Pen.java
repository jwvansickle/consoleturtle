/* John VanSickle Feb 18, 2014 Pen Class
 * This class is just a pen object of a boolean that is true for pen is down or false for pen is up
 */

package edu.cis306.turtle;

public class Pen {
	//Constructor default
	public Pen(){
		isPenDown = false;
	}
	
	private boolean isPenDown;
	
	//function that sets the pen down to leave path
	public void PutPenDown(){
		isPenDown = true;
	}
	
	//function that sets the pen up to not leave path
	public void PutPenUp(){
		isPenDown = false;
	}
	//function that sets pen down to leave path
	public boolean IsPenDown(){
		return isPenDown;
	}
}
