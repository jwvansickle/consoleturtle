/* John VanSickle Feb 18, 2014 Turtle Class
 * This class is for tracking where the turtle currently is as well as maintaining the status of the pen object and floor object through functions that call functions
 * of each other class respectively.
 */

package edu.cis306.turtle;

public class Turtle {
	Turtle(){
		penObj = new Pen();
		floorObj = new Floor();
		currentRow = 0;
		currentColumn = 0;
		currentDirection = 'r';
	}

	private Pen penObj;
	private Floor floorObj;
	private int currentRow;
	private int currentColumn;
	private char currentDirection;
	
	//Functions - the turtle can do what? that should call method in Pen or Floor that takes Turtle arguments and acts on them
	//
	//function to put penobject up
	public void PenUp(){
		penObj.PutPenUp();
	}//end PenUp
	
	//function to put penobject down
	public void PenDown(){
		penObj.PutPenDown();
	}//end PenDown
	
	//function for turtle to move. Calls a function from Floor that needs to pass in currentDirection, and howFarToMove from user input
	//switch statement is needed to maintain the turtle's current positioning relative to floor object
	public void MoveTurtle(int howFarToMove){
		if (floorObj.Move(currentDirection, howFarToMove, currentRow, currentColumn, penObj.IsPenDown())){
			//always need to subtract 1 from how far to move because of arrays starting at 0 not 1
			switch(currentDirection){
			case 'r':
				currentColumn += howFarToMove - 1;
				break;
			case 'd':
				currentRow += howFarToMove - 1;
				break;
			case 'l':
				currentColumn -= howFarToMove - 1;
				break;
			case 'u':
				currentRow -= howFarToMove -1;
				break;
			default:
				break;
			}
		}
		else{
			System.out.println("You cannot move that far in that direction!");
		}
	}
	
	//function to have the floor printed for the turtleObj instance
	public void PrintTheFloor(){
		floorObj.PrintTheFloor();
	}//end of PrintTheFloor
	//overloaded PrintTheFloor to also put down turtle position using '&'
	public void PrintTheFloorAndTurtle(){
		floorObj.PrintTheFloor(currentRow, currentColumn);
	}
	
	//function to turn right; will handle all situations of currentDirection
	public void TurnRight(){
		switch(currentDirection){
		case 'r':
			currentDirection = 'd';
			break;
		case 'd':
			currentDirection = 'l';
			break;
		case 'l':
			currentDirection = 'u';
			break;
		case 'u':
			currentDirection = 'r';
			break;
		default:
			break;
		}
	}//end of TurnRight
	
	//function to turn left; will handle all situation of currentDirection
	public void TurnLeft(){
		switch(currentDirection){
		case 'r':
			currentDirection = 'u';
			break;
		case 'd':
			currentDirection = 'r';
			break;
		case 'l':
			currentDirection = 'd';
			break;
		case 'u':
			currentDirection = 'l';
			break;
		default:
			break;
		}
	}//end of TurnLeft	
}
