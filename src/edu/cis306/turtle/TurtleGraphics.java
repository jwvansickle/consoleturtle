/* John VanSickle Feb 18, 2014 TurtleGraphics Class
 * This class is the driver for Turtle Class which uses Pen and Floor Classes
 * User menu gives options to turn, move forward, display where they have traveled, and quit
 */

package edu.cis306.turtle;

import java.util.Scanner;

public class TurtleGraphics {
	//constructor
	TurtleGraphics(){
		turtleObj = new Turtle();
		userInput = new Scanner(System.in);
	}
	
	Turtle turtleObj;
	Scanner userInput;
	private int selection;
	
	//RunTurtle will perform all operations to run Turtle in a loop
	public void RunTurtle(){

		//Notice to user of default values
		System.out.println("Default position is 0,0\nDefault direction is right\nDefault pen position is up\n");
		
		//selection 9 is to terminate program
		while (selection != 9){
			System.out.println("1: Pen Up");
			System.out.println("2: Pen Down");
			System.out.println("3: Turn Right");
			System.out.println("4: Turn Left");
			System.out.println("5: Move Forward N Spaces");
			System.out.println("6: Display Floor");
			System.out.println("7: Display Floor and Turtle");
			System.out.println("9: Quit");
			
			selection = userInput.nextInt();
			
			switch (selection){
			case 1:
				turtleObj.PenUp();
				break;
			case 2:
				turtleObj.PenDown();
				break;
			case 3:
				turtleObj.TurnRight();
				break;
			case 4:
				turtleObj.TurnLeft();
				break;
			case 5:
				//need additional input for how far to move forward
				System.out.println("How many spaces forward?");
				turtleObj.MoveTurtle(userInput.nextInt());
				break;
			case 6:
				turtleObj.PrintTheFloor();
				break;
			case 7:
				turtleObj.PrintTheFloorAndTurtle();
			case 9:
				break;
			default:
				System.out.println("MAKE A VALID SELECTION!");
				break;
			}
		}
	}//end of RunTurtle
	
	public static void main(String[] args) {
		TurtleGraphics tg = new TurtleGraphics();
		tg.RunTurtle();
		
	}//end of main

}
