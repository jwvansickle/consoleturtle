/* John VanSickle Feb 18, 2014 Floor Class
 * This class maintains the integrity of where the turtle has traveled while the pen is down - ie drawing the path by setting values to 1.
 */
package edu.cis306.turtle;

public class Floor {
	//constructor
	Floor(){
		floor = new int[20][20];
	}
	
	private int[][] floor;
	
	//function that prints an '*' where there is a 1 in the array and prints nothing when a 0
	public void PrintTheFloor(){
		for (int r = 0; r < floor.length; r++){
			for (int c = 0; c < floor[r].length; c++){
				if (floor[r][c] == 1)
					System.out.print('*');
				else 
					System.out.print(' ');
			}//end of inner for loop
			System.out.println();
		}//end of outer for loop
	}//end of printTheFloor function
	//overloaded function
	public void PrintTheFloor(int pCurrentRow, int pCurrentColumn){
		for (int r = 0; r < floor.length; r++){
			for (int c = 0; c < floor[r].length; c++){
				if (r == pCurrentRow && c == pCurrentColumn)
					System.out.print('&');
				else if (floor[r][c] == 1)
					System.out.print('*');
				else 
					System.out.print(' ');
			}//end of inner for loop
			System.out.println();
		}//end of outer for loop
	}//end of printTheFloor function
	
	//function for moving a set number of spots in passed in direction
	//uses switch statement to encompass all directions
	//uses boolean to return whether move was succesful so turtle object can update its current position
	public boolean Move(char pDirectionToMove, int howFarToMove, int pCurrentRow, int pCurrentColumn, boolean pIsPenDown){
		switch (pDirectionToMove){
			case 'r':
				if (pCurrentColumn + howFarToMove > 20){
					//out of bounds of array
					return false;
				}
				else{
					//only set values in array to 1 when pen is down
					if (pIsPenDown){
						for (int c = pCurrentColumn; c < pCurrentColumn + howFarToMove; c++){
							floor[pCurrentRow][c] = 1;
						}
					}
					return true;
				}
			case 'd':
				if (pCurrentRow + howFarToMove > 20){
					//out of bounds of array
					return false;
				}
				else{
					//only set values in array to 1 when pen is down
					if (pIsPenDown){
						for (int r = pCurrentRow; r < pCurrentRow + howFarToMove; r++){
							floor[r][pCurrentColumn] = 1;
						}
					}
					return true;
				}
			case 'l':
				if (pCurrentColumn - howFarToMove < -1){
					//out of bounds of array
					return false;
				}
				else{
					//only set values in array to 1 when pen is down
					if (pIsPenDown){
						for (int c = pCurrentColumn; c > pCurrentColumn - howFarToMove; c--){
							floor[pCurrentRow][c] = 1;
						}
					}
					return true;
				}
			case 'u':
				if (pCurrentRow - howFarToMove < -1){
					//out of bounds of array
					return false;
				}
				else{
					//only set values in array to 1 when pen is down
					if (pIsPenDown){
						for (int r = pCurrentRow; r > pCurrentRow - howFarToMove; r--){
							floor[r][pCurrentColumn] = 1;
						}
					}
					return true;
				}
			default:
				return false;
		}//end of switch statement
	}//end of Move function
}
